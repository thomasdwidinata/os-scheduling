window.scheduleAlgorithm = {
  'FIRST_COME': 0,
  'ROUND_ROBIN_1': 1,
  'ROUND_ROBIN_4': 2,
  'SHORTEST_JOB_NEXT': 3
};

function createTimeHeader(length) {
  let row = '<tr>';

  for(let i = 0; i < length; i++){
    row += '<td>'+i+'</td>';
  }

  return row + '</tr>';
}

let computeCount = 1;

let processList = [];
document.getElementById('processInputForm').addEventListener('submit', function (e) {
  e.preventDefault();

  processList = [];
  let tmpProcess = {
    processName: '',
    processStart: 0,
    duration: 0
  };

  let fields = document.getElementsByClassName('processDetailInput');

  for(let i = 3; i < fields.length; i += 3){
    tmpProcess.processName = fields[i].value;
    tmpProcess.processStart = fields[i+1].value;
    tmpProcess.duration = fields[i+2].value;

    processList.push(JSON.parse(JSON.stringify(tmpProcess)));
  }

  drawAll(processList);
});

document.getElementById('addNewProcess').addEventListener('click', function(){
  newProcessForm();
});

function emptyProcessList() {
  let processes = document.getElementsByClassName('removeProcessButton');

  for(let i = processes.length - 1; i > 0; i--){
    if(processes[i].parentElement.id == 'fieldOriginal')
      continue;

    removeProcess(processes[i]);
  }
}

function randomRange(min, max) {
  return Math.random() * (max - min) + min;
}

document.getElementById('loadRandomProcess').addEventListener('click', function(){
  emptyProcessList();
  let currentCount = 0;

  for(let i = 0; i < Math.round(randomRange(0, 20)); i++){
    newProcessForm();
  }

  let fields = document.getElementsByClassName('processDetailInput');
  for(let i = 3; i < fields.length; i += 3){
    fields[i].value = 'P' + currentCount;
    fields[i+1].value =  Math.round(randomRange(0, 50));
    fields[i+2].value =  Math.round(randomRange(0, 50));
    currentCount++;
  }
});

document.getElementById('loadDefaultProcess').addEventListener('click', function(){
  emptyProcessList();

  let defaultList = [
    {
      processName: 'A',
      processStart: 0,
      duration: 3
    },
    {
      processName: 'B',
      processStart: 1,
      duration: 5
    },
    {
      processName: 'C',
      processStart: 3,
      duration: 2
    },
    {
      processName: 'D',
      processStart: 9,
      duration: 5
    },
    {
      processName: 'E',
      processStart: 12,
      duration: 5
    }
  ]

  for(let i = 0; i < 5; i++){
    newProcessForm();
  }

  let fields = document.getElementsByClassName('processDetailInput');
  for(let i = 0; i < defaultList.length; i++){
    let index = (i + 1) * 3;
    fields[index].value = defaultList[i].processName;
    fields[index+1].value = defaultList[i].processStart;
    fields[index+2].value = defaultList[i].duration;
  }
});

function newProcessForm() {
  let div = document.getElementById('fieldOriginal'),
      clone = div.cloneNode(true); // true means clone all childNodes and all event handlers
  clone.id='';
  document.getElementById('fieldsContainer').appendChild(clone);
}
newProcessForm();

function removeProcess(button) {
  button.parentElement.remove();
}

function clearSchedule() {
  computeCount = 1;
  document.getElementById('tableContent').innerHTML = '<center>Add process and press \'Compute schedule\' to continue</center>';
}

function drawAll(data) {
  if(computeCount == 1)
    document.getElementById('tableContent').innerHTML = '<h6 class="subtitle">Compute #'+computeCount+'</h6>';
  else
    document.getElementById('tableContent').innerHTML += '<hr><br><h6 class="subtitle">Compute #'+computeCount+'</h6>';

  computeCount++;
  draw('Shortest Job Next', 'tableContent', {
    data: JSON.parse(JSON.stringify(data)),
    algorithm: scheduleAlgorithm.SHORTEST_JOB_NEXT
  });
  draw('First Come, First Serve', 'tableContent', {
    data: JSON.parse(JSON.stringify(data)),
    algorithm: scheduleAlgorithm.FIRST_COME
  });
  draw('Round Robin - Time 1', 'tableContent', {
    data: JSON.parse(JSON.stringify(data)),
    algorithm: scheduleAlgorithm.ROUND_ROBIN_1
  });
  draw('Round Robin - Time 4', 'tableContent', {
    data: JSON.parse(JSON.stringify(data)),
    algorithm: scheduleAlgorithm.ROUND_ROBIN_4
  });
}

function draw(name, containerId, toDraw){
  let data = toDraw.data;
  let result;
  let names = [];
  let placeAt = document.getElementById(containerId);
  let cells = [];
  let mainContainer = document.createElement('div');
  let title = document.createElement('h2');
  let runtime = document.createElement('h3');

  title.innerText = name;
  title.classList.add('title');
  title.classList.add('is-4');
  mainContainer.appendChild(title);

  switch (toDraw.algorithm) {
    case scheduleAlgorithm.FIRST_COME: result = firstCome(data); break;
    case scheduleAlgorithm.ROUND_ROBIN_1: result = roundRobin(data, 1); break;
    case scheduleAlgorithm.ROUND_ROBIN_4: result = roundRobin(data, 4); break;
    case scheduleAlgorithm.SHORTEST_JOB_NEXT: result = shortestJob(data); break;
    default:
      throw 'invalid algorithm';
  }

  runtime.innerHTML = 'Function completed in ' + Math.round( result.runtime*1e4 )/1e4 + ' ms';
  runtime.style.marginBottom = '40px';
  runtime.style.marginTop = '-20px';
  mainContainer.appendChild(runtime);

  for(let y = 0; y < data.length + 1; y++){
    let containerTmp = document.createElement('div');
    mainContainer.appendChild(containerTmp);
    containerTmp.className = 'cellContainer';

    cells.push([]);
    for(let x = 0; x < result.result.length + 1; x++){
      let tmp = document.createElement('div');
      tmp.className = 'cell col' + y + ' row' + x;
      containerTmp.appendChild(tmp);

      if(y == 0 && x >= 1){
        tmp.innerHTML = x - 1;
      }

      cells[y].push(tmp);
    }
  }

  cells[0][0].innerHTML = 'Process';

  for (var i = 0; i < data.length; i++) {
    cells[i + 1][0].innerHTML = data[i].processName;
    names.push(data[i].processName);
  }

  for (let i = 0; i < result.result.length; i++) {
    let index = names.indexOf(result.result[i]);

    if(index >= 0)
      cells[index + 1][i + 1].classList.add('currentTime');
  }

  mainContainer.classList.add('tableContainer');
  mainContainer.style.height = (cells.length * 32 + 100) + 'px';
  mainContainer.style.width = (cells[0].length * 32 + 128) + 'px';
  placeAt.appendChild(mainContainer);
}

function firstCome(process) {
  let result = [];
  let queue = [];

  let time = 0;
  let nextProcessToAdd = 0;
  var t0 = performance.now(), t1;

  /* Find the first process and when it is loaded */
  process.sort(function (a, b) {
    return a.processStart - b.processStart;
  });

  queue.push(process[nextProcessToAdd]);
  time = process[nextProcessToAdd].processStart;
  nextProcessToAdd++;

  for(let i = 0; i< time; i++){ // Add blank time block if needed
    result.push(null);
  }

  while(queue.length != 0 || nextProcessToAdd < process.length){
    let selected = queue[0];

    /* Check if a new process must be added */
    for(let i = nextProcessToAdd; i < process.length; i++){
      if(process[i].processStart <= time + 1){
        queue.push(process[i]);
        nextProcessToAdd = i + 1;
      }
    }

    /* Advance in time */
    if(selected != undefined){
      selected.duration--;
      result.push(selected.processName);

      if(selected.duration <= 0){
        queue.splice(0, 1);
      }
    }else{
      result.push(null);
    }
    time++;
  }

  t1 = performance.now();
  return {
    result: result,
    runtime: t1 - t0
  };
}

function shortestJob(process) {

			process.sort(function (a, b) {
				return a.processStart - b.processStart;
			});

      var t0 = performance.now(), t1;

			let result = [];
			let time = 0, tempIndex = 0;
			let queue = [];
			while (true){
				if (!queue[0]){
					if(time > process[process.length-1].processStart){
						break;
					}
				} else {
					if(queue[0].duration <= 0){//dequeues when duration is already at 0
						queue.shift();
					}
				}
				for (let index = 0; index<process.length; index++){
					if(process[index].processStart == time){
						if (!queue[0]){
							queue.push(process[index]);
						} else if (queue[queue.length-1].duration < process[index].duration){
							queue.push(process[index]);
						} else {
						    while (process[index].duration>queue[tempIndex].duration){
								tempIndex++;
							}
							queue.splice(tempIndex-1, 0, process[index]);
							tempIndex = 0;
						}
					}
				}
				if(queue[0]){
					result.push(queue[0].processName);//push the process name
					queue[0].duration--;//decrement the duration
				} else {
					result.push(-1);
				}
				time++;//add the timer
			}

      t1 = performance.now();
			return {
        result: result,
        runtime: t1 - t0
      };
		}

function roundRobin(process, quantumTime) {
  let result = [];
  let queue = [];

  let time = 0;
  let nextProcessToAdd = 0;
  let index = 0;
  var t0 = performance.now(), t1;

  /* Find the first process and when it is loaded */
  process.sort(function (a, b) {
    return a.processStart - b.processStart;
  });

  queue.push(process[nextProcessToAdd]);
  time = process[nextProcessToAdd].processStart;
  nextProcessToAdd++;

  for(let i = 0; i< time; i++){ // Add blank time block if needed
    result.push(null);
  }

  while(queue.length != 0 || nextProcessToAdd < process.length){
    // debugger;
    let selected;
    let doPop = false;
    let newProcess = false;

    /* Check if a new process must be added */
    for(let i = nextProcessToAdd; i < process.length; i++){
      if(process[i].processStart <= time){
        queue.push(process[i]);
        if(!newProcess){
          newProcess = true;
          index = queue.length - 1;
          selected = queue[index];
        }
        nextProcessToAdd = i + 1;
      }
    }

    if(!newProcess)
      selected = queue[index];

    /* Push process to schedule depending of quantum time */
    if(selected != undefined){
      for(let i = 0; i < quantumTime; i++){
        selected.duration--;
        result.push(selected.processName);

        if(selected.duration <= 0){
          queue.splice(index, 1);
          doPop = true;
          break;
        }

        if(i < quantumTime - 1){
          time++
        }
      }
    }else{
      result.push(null);
    }
    time++;

    if(!doPop)
      index++;

    if(index >= queue.length)
      index = 0;
  }

  t1 = performance.now();
  return {
    result: result,
    runtime: t1 - t0
  };
}
